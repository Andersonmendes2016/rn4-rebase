<?php
class names extends model {

	public function listSub($s){
		$data = array();

		$sql = "SELECT * FROM names WHERE subtype = '$s'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function getNameByCod($cod) {
		$data = "";

		$sql = "SELECT name FROM names WHERE cod = '$cod'";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetch()['name'];
		}
		return $data;
	}

	public function newType($name, $codename, $type) {
		if (!empty($name) && !empty($codename)) {
			$name = addslashes($name);
			$codename = addslashes($codename);

			$sql = "INSERT INTO names SET cod = '$codename', name = '$name', subtype = '$type'";
			$this->db->query($sql);
			header("Location: /adm/create");
		}
	}

	public function updateType($id, $codename, $name) {
		if(!empty($id) && !empty($codename) && !empty($name)) {
			$id = addslashes($id);
			$codename = addslashes($codename);
			$name = addslashes($name);

			$sql = "UPDATE names SET cod = '$codename', name = '$name' WHERE id = '$id'";
			$this->db->query($sql);
		}

		header("Location: /adm");
	}

	public function getType($t, $l) {
		$data = array();

		$sql = "SELECT * FROM names WHERE subtype = '$t' ORDER BY id DESC LIMIT $l";
		$sql = $this->db->query($sql);

		if($sql->rowCount() > 0) {
			$data = $sql->fetchAll();
		}
		return $data;
	}

	public function drop($id) {
		if(!empty($id)){
			$id = addslashes($id);
			$sql = "DELETE FROM names WHERE id = '$id'";
			$this->db->query($sql);
		}
	}

	public function getTypeById($id) {
		$data = array();
		if (isset($id) && !empty($id)) {
			$id = addslashes($id);

			$sql = "SELECT * FROM names WHERE id = '$id'";
			$sql = $this->db->query($sql);

			if($sql->rowCount() > 0){
				$data = $sql->fetch();
			}
		}
		return $data;
		print_r($data);exit;
	}

}