<?php if(isset($others) && !empty($others)): ?>
<?php foreach ($others as $other): ?>

<div class="btns">
<a href="/others/builds/<?php echo $other['cod']?>"><button type="button" class="btn btn-secondary btn-lg btn-block" ><?php echo $other['name'] ?></button></a>

<?php endforeach; ?>
</div>
<?php endif; ?>