<?php 
$link="";

if($viewData['session'] == 'roms') {
	$link = '/roms/builds/';
}elseif($viewData['session'] == 'kernels') {
	$link = '/kernels/builds/';
}elseif($viewData['session'] == 'others') {
	$link = '/others/builds/';
}
?>

<style type="text/css">
  a button {
    margin-top:8px;
  }
  a:hover {
    text-decoration: none;
  }
</style>

<div class="btns">
<?php foreach ($roms as $rom) : ?>
<a href="<?php echo $link.$rom['cod'] ?>"><button type="button" class="btn btn-secondary btn-lg btn-block" ><?php echo $rom['name'] ?></button></a>
<?php endforeach?>
