<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="Redmi Note 4 Downloads">
    <meta property="og:title" content="Redmi Note 4 Downloads">
    <meta property="og:description" content="Redmi Note 4 Downloads">
    <meta property="og:site_name" content="Redmi Note 4 Downloads">
    <meta property="og:image" content="/assets/img/logo.png" />
    <meta name="keywords" content="mido,redmi note 4,redmi note 4x,xiaomi,roms,custom roms,miui,download,aosp,oreo,android,pie">
    <meta name="author" content="Anderson Mendes">
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#2D2D2D">
    <meta name="msapplication-TileImage" content="/assets/img/ms-icon-144x144.png">
    <meta name="theme-color" content="#2D2D2D">
    <link href="https://fonts.googleapis.com/css?family=Product+Sans" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/assets/css/page.css">
    <title>RN4D - <?php echo $viewData['page']; ?></title>
  </head>
  <body>

    <div class="container">
    	<nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="/adm">
            <img src="/assets/img/logo.png" width="50" alt="logo">RN4Downloads - ADM</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav ">
          <li class="nav-item ">
            <a class="nav-link <?if($viewData['session']=='home'){echo'active';}?>" href="/">Home</a>
          </li>
          <li class="nav-item <?if($viewData['session']=='create'){echo'active';}?>">
            <a class="nav-link" href="/adm/create">Create</a>
          </li>
          <li class="nav-item <?if($viewData['session']=='edit'){echo'active';}?>">
            <a class="nav-link" href="/adm/edit">Edit</a>
          </li>
          <li class="nav-item <?if($viewData['session']=='delete'){echo'active';}?>">
            <a class="nav-link" href="/adm/delete">Delete</a>
          </li>
          ---
            <li class="nav-item">
            <a class="nav-link" href="/adm/logout">Logout</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="alert alert-dark" role="alert"><?php echo $viewData['page']; ?></div>

    <div class="container">
	<?php $this->loadView($viewName, $viewData); ?>
	</div>
    </br>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/cpage.js"></script>
  </body>
</html>
