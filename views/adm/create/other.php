<form method="POST">
  <div class="form-group">
    <label for="exampleFormControlInput1">Link</label>
    <input type="link" class="form-control" name="link" id="link" placeholder="https://example.com">

    <label for="exampleFormControlInput1">Name</label>
    <input type="text" class="form-control" id="name" placeholder="kernel_blablabla.zip" name="name">

     <label for="exampleFormControlSelect1">SubType</label>
    <select class="form-control" id="exampleFormControlSelect1" name="othertype">
      <?php foreach ($others as $other): ?>
      <option value="<?php echo $other['cod'] ?>"> <?php echo $other['name'] ?></option>
    <?php endforeach;?>
    </select>
    <label for="exampleFormControlInput1">Source</label>
    <input type="text" class="form-control" placeholder="xda" name="source">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Changelog or description</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="changelog"></textarea>
  </div>
      <button class="btn btn-secondary" id="push">PUSH</button>
</form>
<button class="btn btn-secondary" id="magic">AFH MAGIC</button>
