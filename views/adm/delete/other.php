<?php if (isset($roms) && !empty($roms)): ?>
<h3 style="color: white; margin: 10px;">Latest Builds</h3>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Type</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($roms as $rom): ?>
    <tr>
      <th><?php echo $rom['id'] ?></th>
      <td><?php echo $rom['name'] ?></td>
      <td><?php echo $rom['othertype'] ?></td>

    </tr>
<?php endforeach; ?>
  </tbody>
</table>

<form method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">type id</label>
    <input type="text" class="form-control" aria-describedby="id" placeholder="id" name="id">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">type id again</label>
    <input type="text" class="form-control" placeholder="id again" name="id2">
  </div>
  <button type="submit" class="btn btn-danger">Delete</button>
</form>

<?php endif; ?>
