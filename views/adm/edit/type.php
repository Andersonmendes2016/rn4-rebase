<?php if (isset($types) && !empty($types)): ?>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Codename</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($types as $type): ?>
    <tr>
      <th><?php echo $type['id'] ?></th>
      <td><?php echo $type['name'] ?></td>
      <td><?php echo $type['cod'] ?></td>
      <td><a href="/adm/edittype/<?php echo $type['id'] ?>"><button type="button" class="btn btn-warning">Edit</button></a></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
