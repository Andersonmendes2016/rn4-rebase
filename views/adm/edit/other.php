<?php if (isset($others) && !empty($others)): ?>
<h3 style="color: white; margin: 10px;">Latest Builds</h3>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Type</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($others as $other): ?>
    <tr>
      <th><?php echo $other['id'] ?></th>
      <td><?php echo $other['name'] ?></td>
      <td><?php echo $other['othertype'] ?></td>
      <td><a href="/adm/editother/<?php echo $other['id'] ?>"><button type="button" class="btn btn-warning">Edit</button></a></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
