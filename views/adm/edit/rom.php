<?php if (isset($roms) && !empty($roms)): ?>
<h3 style="color: white; margin: 10px;">Latest Builds</h3>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($roms as $rom): ?>
    <tr>
      <th><?php echo $rom['id'] ?></th>
      <td><?php echo $rom['name'] ?></td>
      <td><?php echo $rom['date'] ?></td>
      <td><a href="/adm/editrom/<?php echo $rom['id']."/".$typer ?>"><button type="button" class="btn btn-warning">Edit</button></a></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
