<style type="text/css">
  a button {
    margin-top:8px;
  }
  a:hover {
    text-decoration: none;
  }
</style>
<div class="btns">
<div class="alert alert-dark warn" role="alert">ROMS</div>
<a href="/adm/delete/rom"><button type="button" class="btn btn-secondary btn-lg btn-block" >Delete ROM</button></a>
<a href="/adm/delete/romtypeaosp"><button type="button" class="btn btn-secondary btn-lg btn-block" >Delete RomType AOSP</button></a>
<a href="/adm/delete/romtypemiuic"><button type="button" class="btn btn-secondary btn-lg btn-block" >Delete RomType MIUI CUSTOM</button></a>
<a href="/adm/delete/romtypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block" >Delete RomType MIUI STOCK</button></a>
<div class="alert alert-dark warn" role="alert">KERNELS</div>
<a href="/adm/delete/kernel"><button type="button" class="btn btn-secondary btn-lg btn-block">Delete Kernel</button></a>
<a href="/adm/delete/kerneltype"><button type="button" class="btn btn-secondary btn-lg btn-block">Delete KernelType AOSP</button></a>
<a href="/adm/delete/kerneltypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block">Delete KernelType MIUI</button></a>
<div class="alert alert-dark warn" role="alert">OTHERS</div>
<a href="/adm/delete/other"><button type="button" class="btn btn-secondary btn-lg btn-block">Delete Other</button></a>
<a href="/adm/delete/othertype"><button type="button" class="btn btn-secondary btn-lg btn-block">Delete OtherType</button></a>
</div>