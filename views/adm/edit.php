<style type="text/css">
  a button {
    margin-top:8px;
  }
  a:hover {
    text-decoration: none;
  }
</style>
<div class="btns">
<div class="alert alert-dark warn" role="alert">ROMS</div>

<a href="/adm/edit/romaosp"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit Rom - AOSP</button></a>
<a href="/adm/edit/rommiuic"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit Rom - MIUI CUSTOM</button></a>
<a href="/adm/edit/rommiui"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit Rom - MIUI STOCK</button></a>
</br>
<a href="/adm/edit/romtypeaosp"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit RomType - AOSP</button></a>
<a href="/adm/edit/romtypemiuic"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit RomType - MIUI CUSTOM</button></a>
<a href="/adm/edit/romtypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block" >Edit RomType - MIUI STOCK</button></a>
<div class="alert alert-dark warn" role="alert">KERNELS</div>
<a href="/adm/edit/kernel"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit Kernel AOSP</button></a>
<a href="/adm/edit/kernelmiui"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit Kernel MIUI</button></a>
</br>
<a href="/adm/edit/kerneltype"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit KernelType - AOSP</button></a>
<a href="/adm/edit/kerneltypemiui"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit KernelType - MIUI</button></a>
<div class="alert alert-dark warn" role="alert">OTHERS</div>
<a href="/adm/edit/other"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit Other</button></a>
</br>
<a href="/adm/edit/othertype"><button type="button" class="btn btn-secondary btn-lg btn-block">Edit OtherType</button></a>
</br>
</div>