<?php
class controller {
	public function loadView($viewName, $viewData = array()){
		extract($viewData);

		if(!strpos("_",$viewName)) {
			$viewName = str_replace("_", "/", $viewName);
		}
		
		include 'views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()){
		include 'views/template.php';
	}

	public function loadTemplateInFolder($folder, $viewName, $viewData = array()){
		$viewName = $folder.'/'.$viewName;
		include 'views/'.$folder.'/template.php';
	}
}