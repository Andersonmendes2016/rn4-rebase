<?php

class admController extends controller {

	public function index() {
		adm::islogged();
		$data = array();
		$data['page'] = "ADM - HOME";
		$data['session'] = "home";
		$data['user'] = $_SESSION['rn4ADM']['name'];
		$this->loadTemplateInFolder('adm','home',$data);
		
	}

	public function create($type='') {
		adm::islogged();
		$data = array();
		$data['page'] = "Create";
		$data['session'] = "create";
		$n = new names();

		if(!empty($type)) {

			if($type == 'romaosp') {
				// CREATE ROM AOSP //
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$r = new roms();
					$r->pushRom($_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['romtype']);
				}
				$data['page'] = "Create ROM AOSP";
				$data['roms'] = $n->listSub("rom");
				$this->loadTemplateInFolder('adm','create_rom',$data);
			}elseif($type == 'rommiuic') {
				// CREATE ROM MIUI CUSTOM //
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$r = new roms();
					$r->pushRom($_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['romtype']);
				}
				$data['page'] = "Create ROM MIUI CUSTOM";
				$data['roms'] = $n->listSub("custom");
				$this->loadTemplateInFolder('adm','create_rom',$data);
			}elseif($type == 'rommiui') {
				// CREATE ROM MIUI STOCK//
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$r = new roms();
					$r->pushRom($_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['romtype']);
				}
				$data['page'] = "Create ROM MIUI STOCK";
				$data['roms'] = $n->listSub("stock");
				$this->loadTemplateInFolder('adm','create_rom',$data);
			}elseif($type == 'other'){
				// CREATE OTHER //
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$o = new others();
					$o->push($_POST['name'], $_POST['source'], 
						$_POST['changelog'], $_POST['othertype'],$_POST['link']);
				}
				$data['page'] = "Create Other";
				$data['others'] = $n->listSub("other");
				$this->loadTemplateInFolder('adm','create_other',$data);
			}elseif($type == 'kernel'){
				// CREATE KERNEL //
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$k = new kernels();
					$k->pushKernel($_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['kerneltype']);
				}
				$data['page'] = "Create Kernel AOSP";
				$data['kernels'] = $n->listSub("kernel");
				$this->loadTemplateInFolder('adm','create_kernel',$data);
			}elseif($type == 'kernelmiui'){
				// CREATE KERNEL //
				if(isset($_POST['link']) && isset($_POST['name']) && !empty($_POST['link'])){
					$k = new kernels();
					$k->pushKernel($_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['kerneltype']);
				}
				$data['page'] = "Create Kernel MIUI";
				$data['kernels'] = $n->listSub("kernel_miui");
				$this->loadTemplateInFolder('adm','create_kernel',$data);
			}if($type == 'romtypeaosp'){
				// CREATE ROM TYPE AOSP //
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'rom');
				}
				$data['page'] = "Create RomType AOSP";
				$data['types'] = $n->listSub("rom");
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}if($type == 'romtypemiuic'){
				// CREATE ROM TYPE MIUI CUSTOM//
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'custom');
				}
				$data['page'] = "Create RomType MIUI Custom";
				$data['types'] = $n->listSub("custom");
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}if($type == 'romtypemiui'){
				// CREATE ROM TYPE MIUI STOCK //
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'stock');
				}
				$data['page'] = "Create RomType MIUI Stock";
				$data['types'] = $n->listSub("stock");
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}elseif($type == 'kerneltype'){
				// CREATE KERNEL TYPE AOSP//
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'kernel');
				}
				$data['page'] = "Create KernelType AOSP";
				$data['types'] = $n->listSub("kernel");
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}elseif($type == 'kerneltypemiui'){
				// CREATE KERNEL TYPE MIUI //
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'kernel_miui');
				}
				$data['page'] = "Create KernelType MIUI";
				$data['types'] = $n->listSub("kernel_miui");
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}elseif($type == 'othertype'){
				// CREATE OTHER TYPE //
				if (isset($_POST['name']) && isset($_POST['codename'])) {
					$n->newType($_POST['name'], $_POST['codename'],'other');
				}
				$data['types'] = $n->listSub("other");
				$data['page'] = "Create OtherType";
				$this->loadTemplateInFolder('adm','create_newtype',$data);
			}
		}else{
			$this->loadTemplateInFolder('adm','create',$data);
		}
		
	}
	public function edit($type='') {
		$n = new names();
		adm::islogged();
		$data = array();
		if(!empty($type)){
			if($type == 'romaosp') {
				// EDIT ROM //
				$r = new roms();
				$data['page'] = "Edit ROM AOSP";
				$data['session'] = "edit";
				$data['roms'] = $r->getUpdatesbyType('rom',5);
				$data['typer'] = "rom";
				$this->loadTemplateInFolder('adm','edit_rom',$data);
			}elseif($type == 'rommiuic') {
				// EDIT ROM MIUI CUSTOM //
				$r = new roms();
				$data['page'] = "Edit ROM MIUI CUSTOM";
				$data['session'] = "edit";
				$data['roms'] = $r->getUpdatesbyType('custom',5);
				$data['typer'] = "custom";
				$this->loadTemplateInFolder('adm','edit_rom',$data);
			}elseif($type == 'rommiui') {
				// EDIT ROM MIUI STOCK //
				$r = new roms();
				$data['page'] = "Edit ROM MIUI STOCK";
				$data['session'] = "edit";
				$data['roms'] = $r->getUpdatesbyType('stock',5);
				$data['typer'] = "stock";
				$this->loadTemplateInFolder('adm','edit_rom',$data);
			}elseif($type == 'kernel'){
				// EDIT KERNEL AOSP //
				$k = new kernels();
				$data['page'] = "Edit Kernel";
				$data['session'] = "edit";
				$data['kernels'] = $k->getUpdatesbyType('kernel', 5);
				$data['typer'] = "kernel";
				$this->loadTemplateInFolder('adm','edit_kernel',$data);
			}elseif($type == 'kernelmiui'){
				// EDIT KERNEL MIUI //
				$k = new kernels();
				$data['page'] = "Edit Kernel";
				$data['session'] = "edit";
				$data['kernels'] = $k->getUpdatesbyType('kernel_miui', 5);
				$data['typer'] = "kernel_miui";
				$this->loadTemplateInFolder('adm','edit_kernel',$data);
			}elseif($type == 'other'){
				// EDIT OTHER //
				$o = new others();
				$data['page'] = "Edit Other";
				$data['session'] = "edit";
				$data['others'] = $o->getupdates(5);
				$this->loadTemplateInFolder('adm','edit_other',$data);				
			}elseif($type == 'romtypeaosp'){
				// EDIT ROMTYPE AOSP //
				$data['page'] = "Edit RomType AOSP";
				$data['types'] = $n->listSub("rom");
				$data['session'] = "edit";
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}elseif($type == 'romtypemiuic'){
				// EDIT ROMTYPE MIUI CUSTOM //
				$data['page'] = "Edit RomType MIUI CUSTOM";
				$data['types'] = $n->listSub("custom");
				$data['session'] = "edit";
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}elseif($type == 'romtypemiui'){
				// EDIT ROMTYPE MIUI //
				$data['page'] = "Edit RomType MIUI";
				$data['types'] = $n->listSub("stock");
				$data['session'] = "edit";
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}elseif($type == 'kerneltype'){
				// EDIT KERNELTYPE //
				$data['page'] = "Edit KernelType AOSP";
				$data['types'] = $n->listSub("kernel");
				$data['session'] = "edit";
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}elseif($type == 'kerneltypemiui'){
				// EDIT KERNELTYPE //
				$data['page'] = "Edit KernelType MIUI";
				$data['types'] = $n->listSub("kernel_miui");
				$data['session'] = "edit";
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}elseif($type == 'othertype'){
				// EDIT OTHERTYPE //
				$data['page'] = "Edit OtherType";
				$data['session'] = "edit";
				$data['types'] = $n->listSub("other");
				$this->loadTemplateInFolder('adm','edit_type',$data);				
			}
		}else{
			$data['page'] = "Edit";
			$data['session'] = "edit";
			$this->loadTemplateInFolder('adm','edit',$data);
		}
	}

	public function editrom($id,$type='') {
		adm::islogged();
		$r = new roms();
		if(isset($_POST['name']) && isset($_POST['link']) && isset($_POST['romtype'])) {
			$r->edit($id, $_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['romtype']);
		}
		$data = array();
		$n = new names();
		$data['rom'] = $r->getbuildbyid($id);
		if(!empty($type)){
			$data['types'] = $n->listSub($type);
		}else{
			$data['types'] = $n->listSub("rom");
		}
		
		$data['page'] = "Edit ROM";
		$data['session'] = 'edit';
		$this->loadTemplateInFolder('adm','edit_rom-form',$data);
	}

	public function editkernel($id,$type='') {
		adm::islogged();
		$n = new names();
		$k = new kernels();
		if(isset($_POST['name']) && isset($_POST['link']) && isset($_POST['kerneltype'])) {
			$k->edit($id, $_POST['name'], $_POST['dev'], $_POST['source'],
						$_POST['link'], $_POST['changelog'], $_POST['kerneltype']);
		}
		$data = array();
		$data['kernel'] = $k->getbuildbyid($id);
		if(!empty($type)){
			$data['types'] = $n->listSub($type);
		}else{
			$data['types'] = $n->listSub("kernel");
		}
		$data['page'] = "Edit Kernel";
		$data['session'] = 'edit';
		$this->loadTemplateInFolder('adm','edit_kernel-form',$data);	
	}

	public function editother($id) {
		adm::islogged();
		$n = new names();
		$o = new others();
		if(isset($_POST['name']) && isset($_POST['link']) && isset($_POST['othertype'])) {
			$o->edit($id, $_POST['name'], $_POST['source'], $_POST['changelog'], $_POST['othertype'], $_POST['link']);
		}
		$data = array();
		$data['other'] = $o->getbuildbyid($id);
		$data['types'] = $n->listSub("other");
		$data['page'] = "Edit Other";
		$data['session'] = 'edit';
		$this->loadTemplateInFolder('adm','edit_other-form',$data);	
	}

	public function edittype($id) {
		adm::islogged();
		$n = new names();
		if(isset($_POST['name']) && isset($_POST['codename'])){
			$n->updateType($id, $_POST['codename'], $_POST['name']);
		}
		$data = array();
		$data['type'] = $n->getTypeById($id);
		$data['page'] = "Edit Type";
		$data['session'] = 'edit';
		$this->loadTemplateInFolder('adm','edit_type-form',$data);	
	}

	public function delete($type='') {
		adm::islogged();
		$n = new names();
		if(!empty($type)){
			if($type == 'rom'){
				// DELETE ROM //
				$r = new roms();
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$r->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete ROM";
				$data['session'] = "delete";
				$data['roms'] = $r->getupdates(5);
				$this->loadTemplateInFolder('adm','delete_rom',$data);
			}elseif($type == 'kernel') {
				// DELETE KERNEL //
				$k = new kernels();
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$k->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete Kernel";
				$data['session'] = "delete";
				$data['roms'] = $k->getupdates(5);
				$this->loadTemplateInFolder('adm','delete_rom',$data);
			}elseif($type == 'other'){
				// DELETE OTHER //
				$o = new others();
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$o->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete Other";
				$data['session'] = "delete";
				$data['roms'] = $o->getupdates(5);
				$this->loadTemplateInFolder('adm','delete_other',$data);
			}elseif($type == 'romtypeaosp'){
				// DELETE ROMTYPE AOSP//
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete RomType AOSP";
				$data['session'] = "delete";
				$data['types'] = $n->getType('rom',5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}elseif($type == 'romtypemiuic'){
				// DELETE ROMTYPE MIUI CUSTOM //
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete RomType MIUI Custom";
				$data['session'] = "delete";
				$data['types'] = $n->getType('custom',5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}elseif($type == 'romtypemiui'){
				// DELETE ROMTYPE MIUI STOCK //
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete RomType MIUI STOCK";
				$data['session'] = "delete";
				$data['types'] = $n->getType('stock',5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}elseif($type == 'kerneltype'){
				// DELETE KERNELTYPE //
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete KernelType AOSP";
				$data['session'] = "delete";
				$data['types'] = $n->getType('kernel',5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}elseif($type == 'kerneltypemiui'){
				// DELETE KERNELTYPE MIUI//
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete KernelType MIUI";
				$data['session'] = "delete";
				$data['types'] = $n->getType('kernel_miui',5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}elseif($type == 'othertype'){
				// DELETE OTHERTYPE //
				if(isset($_POST['id']) && isset($_POST['id2'])){
					if($_POST['id'] == $_POST['id2']){
						$n->drop($_POST['id']);
						header("Location: /adm");
					}
				}
				$data['page'] = "Delete OtherType";
				$data['session'] = "delete";
				$data['types'] = $n->getType('other', 5);
				$this->loadTemplateInFolder('adm','delete_type',$data);
			}
		}else{
			// LOAD MAIN DELETE PAGE //
			$data['page'] = "Delete";
			$data['session'] = "delete";
			$this->loadTemplateInFolder('adm','delete',$data);
		}
	}

	public function login() {
		if (isset($_POST['email']) && !empty($_POST['email'])) {
			$a = new adm();
			$a->login($_POST['email'], $_POST['password']);
		}else{
			$data = array();
			$data['page'] = "Login";
			$data['session'] = "adm";
			$this->loadTemplate('login', $data);
		}
	}

	public function logout() {
		adm::islogged();
		adm::logout();
		header("Location: /adm");
	}
}