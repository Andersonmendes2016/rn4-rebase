<?php

class aboutController extends controller {

	public function index() {
		$data = array();
		$data['page'] = "About Website";
		$data['session'] = 'about';
		$this->loadTemplate('about',$data);
	}
}