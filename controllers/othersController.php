<?php

class othersController extends controller {
	
	public function index() {
		$n = new names();
		$data = array();
		$data['roms'] = $n->listSub("other");
		$data['page'] = "Others";
		$data['session'] = 'others';
		$this->loadTemplate('builds',$data);
	}

	public function builds($cod) {
		$data = array();
		$o = new others();
		$n = new names();
		$data['updates'] = $o->getUpdatesbyCod($cod);
		$data['page'] = $n->getNameByCod($cod)." Builds";
		$data['session'] = 'others';
		$this->loadTemplate('build',$data);
	}

}