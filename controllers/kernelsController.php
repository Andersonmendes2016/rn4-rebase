<?php

class kernelsController extends controller {

	public function index() {
		$data = array();
		$data['page'] = "All Kernels";
		$data['session'] = 'kernels';
		$this->loadTemplate('kernels',$data);
	}

	public function aosp() {
		$data = array();
		$data['page'] = "Kernel for AOSP Roms";
		$n = new names();
		$data['roms'] = $n->listSub("kernel");
		$data['session'] = 'kernels';
		$this->loadTemplate('builds',$data);
	}

	public function miui() {
		$data = array();
		$data['page'] = "Kernel for MIUI Roms";
		$n = new names();
		$data['roms'] = $n->listSub("kernel_miui");
		$data['session'] = 'kernels';
		$this->loadTemplate('builds',$data);
	}

	public function builds($cod) {
		$data = array();
		$k = new kernels();
		$n = new names();
		$data['updates'] = $k->getUpdatesbyCod($cod);
		$data['page'] = $n->getNameByCod($cod)." Builds";
		$data['session'] = 'kernels';
		$this->loadTemplate('build',$data);
	}
}