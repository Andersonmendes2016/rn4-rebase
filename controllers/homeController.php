<?php

class homeController extends controller {

	public function index() {
		$data = array();
		$h = new home();
		$data['updates'] = $h->getUpdates(20);
		$data['page'] = "Latest Updates";
		$data['session'] = 'home';
		$this->loadTemplate('home',$data);
	}
}